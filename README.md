# FabAccess

## Présentation

FabAccess est un ensemble d'outils de gestion à destination du FabLab.
Il permet de faciliter :
- l’accès et le fonctionnement du FabLab par ses usagers.
- la gestion du FabLab par ses administrateurs.

FabAccess est composé de trois programmes :
- une application web Django
- un client NodeMCU
- un client RaspberryPi

FabAccess est un projet opensource fourni avec une documentation détaillée à l'attention des utilisateurs et des développeurs.

## [Application FabAccess](https://gitlab.com/fabaccess/fabaccess-serveur-django)

* Gestion des machines du FabLab
	* Profil des machines / Disponibilité
	* Gestion des incidents
	* Statistiques d'utilisation et d'incidents
	
* Gestion des membres du FabLab
	* Liste des membres présents au FabLab
	* Ajout de membre
	* Import de membres via l'API Dolibarr
	* Statistiques des membres
	
[Lien vers le dépôt](https://gitlab.com/fabaccess/fabaccess-serveur-django)

## [Client NodeMCU](https://gitlab.com/fabaccess/fabaccess-client-nodemcu)

* Lecture de cartes RFID
* Gestion du pilotage des machines en fonction des habilitations de l'utilisateur
* Deux modes d'utilisation possibles en fonction du type de machines

[Lien vers le dépôt](https://gitlab.com/fabaccess/fabaccess-client-nodemcu)

## [Client RaspberryPi](https://gitlab.com/fabaccess/fabaccess-client-rpi)

* Lecture de cartes RFID
* Navigation dans l'application FabAccess avec synthèse vocale pour guider l'utilisateur
* Attribution des cartes RFID aux membres du FabLab

[Lien vers le dépôt](https://gitlab.com/fabaccess/fabaccess-client-rpi)

